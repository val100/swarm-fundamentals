# Docker Swarm
---

## Create and configure the swarm cluster

 - Start the swarm cluster by run the command in the master node:
```
docker swarm init
```

 - Copy the output of the command and run it in the worker node:
```
swarm join --token <token> <server-ip>:2377
```
 
 - Note: you can run the command below in the master to retrieve the command used to join worker nodes:
```
docker swarm join-token worker
```

 - Ensure that both nodes are successfully connected by run the command below from the master node:
```
docker node ls
```

 - Confirms that no service is running in the cluster:
```
docker service ls
```

## Deploy a single service

 - Deploy a visualizer dashboard as a swarm service using the command below:
```
docker service create --replicas 1 --name dashboard --constraint node.role==manager --mount source=/var/run/docker.sock,target=/var/run/docker.sock,type=bind -p 8080:8080 dockersamples/visualizer:stable
```

 - Ensure the service is up and running:
```
docker service ls
```

 - Browse to the dashboard:
```
http://<master-node>:8080
```

## Deploy a example microservice application

 - Create a new folder to store our swarm service:
```
mkdir ~/swarm
```

 - Create a file "docker-stack.yml" to define the swarm stack with the content below:
```
nano ~/swarm/docker-stack.yml
```
```
version: "3"
services:

  redis:
    image: redis:alpine
    ports:
      - "6379"
    networks:
      - frontend
    deploy:
      replicas: 1
      update_config:
        parallelism: 2
        delay: 10s
      restart_policy:
        condition: on-failure
  db:
    image: postgres:9.4
    volumes:
      - db-data:/var/lib/postgresql/data
    networks:
      - backend
    deploy:
      placement:
        constraints: [node.role == manager]
  vote:
    image: dockersamples/examplevotingapp_vote:before
    ports:
      - 5000:80
    networks:
      - frontend
    depends_on:
      - redis
    deploy:
      replicas: 1
      update_config:
        parallelism: 2
      restart_policy:
        condition: on-failure
  result:
    image: dockersamples/examplevotingapp_result:before
    ports:
      - 5001:80
    networks:
      - backend
    depends_on:
      - db
    deploy:
      replicas: 1
      update_config:
        parallelism: 2
        delay: 10s
      restart_policy:
        condition: on-failure

  worker:
    image: dockersamples/examplevotingapp_worker
    networks:
      - frontend
      - backend
    deploy:
      mode: replicated
      replicas: 1
      labels: [APP=VOTING]
      restart_policy:
        condition: on-failure
        delay: 10s
        max_attempts: 3
        window: 120s
      placement:
        constraints: [node.role == manager]

networks:
  frontend:
  backend:

volumes:
  db-data:
```

 - Deploy the "voting-app" stack by run the command below:
```
docker stack deploy --compose-file ~/swarm/docker-stack.yml voting-app
```

 - Access to the "vote" service:
```
http://<master-node-ip>:5000
```

 - Access to the "result" service:
```
http://<master-node-ip>:5000
```

## Inspect a service in the swarm

 - Inspect the "vote" service from the "voting-app" stack:
```
docker service inspect voting-app_vote
```

 - Run the command below to see which nodes are running the "result" service:
```
docker service ps voting-app_result
```

 - Access to the dashboard to see the deployed services:
```
http://<master-node-ip>:8080
```

## Scale a service

 - Scale the "vote" service from "1" to "5" using the command:
```
docker service scale voting-app_vote=5
```

 - Access to the dashboard to see the deployed services:
```
http://<master-node-ip>:8080
```

 - Inspect which services are running from the command line:
```
docker service ls
```

 - Access to the "vote" service:
```
http://<master-node-ip>:5000
```

 - Refresh the page to see how the container ID changes	automatically (automatic load balancing)

---

## Rolling upgrade

 - Upgrade the "result" service using the commands below:
```
docker service update --image selaworkshops/result-service:updated voting-app_result
```

 - Browse to the "result" service to check that the image was updated:
```
http://<master-node-ip>:5001
```

 - Let's update the "vote" service using rolling upgrades with 5 seconds of update delay:
```
docker service update --image selaworkshops/vote-service:updated --update-delay 5s voting-app_vote
```

 - Browse to the "vote" service and refresh many times to see how containers are updated using rolling upgrade:
```
http://<master-node-ip>:5000
```

---

## Rollback a service

 - Let's rollback the "vote" and the "result" services:
```
docker service update --rollback voting-app_vote
```
```
docker service update --rollback voting-app_result
```

 - Browse to the "result" service to check that the image was updated:
```
http://<master-node-ip>:5001
```

 - Browse to the "vote" service to check that the image was updated:
```
http://<master-node-ip>:5000
```

---

 ## cleanup

 - To remove the voting-app services use the command below:
```
docker stack rm voting-app
```

 - Access to the dashboard to see the running services:
```
http://<master-node-ip>:8080
```

 - To remove the dashboard services use the command below:
```
docker service rm dashboard
```

 - Inspect which services are running:
```
docker service ls
```